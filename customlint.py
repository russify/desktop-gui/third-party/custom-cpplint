import sys
from io import StringIO

import cpplint


ignored = {'legal/copyright'}


def customize(stream, row: str):
    if not row.endswith(']'):
        return False

    code_begin = row.rfind('[')
    if row[row.rfind('[', None, code_begin) + 1:row.rfind(']', None, code_begin)] in ignored:
        cpplint._cpplint_state.error_count -= 1
        return True

    row_begin = row.find(":", row.find(":") + 1)
    row_end = row.find(":", row_begin + 1)
    stream.write('%s(%s, 1)%s\n'
                 % (row[0:row_begin], row[row_begin + 1:row_end], row[row_end:]))

    return True


err_code = None


def exit_interceptor(code):
    global err_code
    err_code = code


def main():
    stdout = sys.stdout
    sys.stdout = output = StringIO()

    stderr = sys.stderr
    sys.stderr = err = StringIO()

    normal_exit = sys.exit
    sys.exit = exit_interceptor

    cpplint.main()

    for line in output.getvalue().split('\n'):
        if not line.startswith("Total errors found:") and len(line) > 0:
            stdout.write(line + '\n')

    if err_code is str and err_code.upper().startswith('FATAL'):
        normal_exit(err_code)

    for line in err.getvalue().split('\n'):
        if not customize(stderr, line):
            stderr.write(line + '\n')

    stdout.write('Total errors found:' + str(cpplint._cpplint_state.error_count) + '\n')

    normal_exit(cpplint._cpplint_state.error_count > 0)


main()